#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <time.h>
#include <netdb.h>
#include <netinet/in.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>


/* TODO: Put this params into a separeted .h file */
#define LISTENQ 1
#define MAXDATASIZE 100
#define MAXLINE 4096

/*Configuration paramaters*/
#define PORT_PARAMATER_ORDER 1
#define CLIENTS_PARAMATER_ORDER 2

/* TODO: Put this ERRORS macros into a separeted errors.h file */
#define SOCKET_FAILURE 2
#define BIND_FAILURE 3

void welcome_message();
void verify_arguments(int argc, char** argv);
int create_socket(struct sockaddr_in* server, char* send_buffer);
void verify_socket_creation(int listen_id);
void configure_server_socket(struct sockaddr_in* server, int listen_id, char** argv);
void verify_socket_bind(struct sockaddr_in* server, int listen_id);

int main(int argc, char** argv)
{
  int listen_id = 0;
  struct sockaddr_in server;
  char send_buffer[MAXLINE];
  int maximum_number_of_clients;

  verify_arguments(argc, argv);
  welcome_message();

  listen_id = create_socket(&server, send_buffer);
  configure_server_socket(&server, listen_id, argv);

  maximum_number_of_clients = atoi(argv[CLIENTS_PARAMATER_ORDER]);
  listen(listen_id, maximum_number_of_clients);

  return EXIT_SUCCESS;
}

void welcome_message()
{
  printf("\n");
  printf("=== Welcome to Vortoj IRC Server ===\n");
  printf("\n");
  printf("Developed by:\n");
  printf("- Arthur Del Esposte\n");
  printf("- Athos Ribeiro\n");
  printf("\n");
}

void verify_arguments(int argc, char** argv)
{
  if(argc != 3)
  {
    fprintf(stderr, "Error: need exactly 1 argument. Usage:\n  $ server <port_number> <maximum_number_of_clients>\n");
    exit(EXIT_FAILURE);
  }
}

int create_socket(struct sockaddr_in* server, char* send_buffer)
{
  int listen_id;

  memset(server, '0', sizeof(*server));
  memset(send_buffer, '0', sizeof(*send_buffer));

  listen_id = socket(AF_INET, SOCK_STREAM, 0);
  verify_socket_creation(listen_id);

  return listen_id;
}

void verify_socket_creation(int listen_id)
{
  if(listen_id == -1)
  {
    fprintf(stderr, "Error: could not create server socket.");
    exit(SOCKET_FAILURE);
  }
}

void configure_server_socket(struct sockaddr_in* server, int listen_id, char** argv)
{
  int port_number = atoi(argv[PORT_PARAMATER_ORDER]);

  server->sin_family = AF_INET;
  server->sin_addr.s_addr = htonl(INADDR_ANY);
  server->sin_port = htons(port_number);

  verify_socket_bind(server, listen_id);
}

void verify_socket_bind(struct sockaddr_in* server, int listen_id)
{
  int result = bind(listen_id, (struct sockaddr *) server, sizeof(*server));
  if(result == -1)
  {
    fprintf(stderr, "Error: could not bind on the Server.");
    exit(BIND_FAILURE);
  }
}
