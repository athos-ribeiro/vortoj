# Vortoj - IRC RFC Compilation

Vortoj IRC server, that means "Words" in Esperanto. It is an implementation of [RFC 1459] (https://tools.ietf.org/html/rfc1459#section-1.3.1)

## 1. Labels

### 1.1 Servers

The server forms the backbone of IRC, providing a point to which clients may connect to to talk to each other, and a point for other servers to connect to, forming an IRC network.

### 1.2 Clients

A client is anything connecting to a server that is not another server.

* Each client is distinguished from other clients by a unique nickname having a maximum length of nine (9) characters
* In addition to the nickname, all servers must have the following information about all clients:
  * the real name of the host that the client is running on
  * the username of the client on that host
  * the server to which the client is connected.

### 1.3 Operators

O perators should be able to perform basic network tasks such as disconnecting and reconnecting servers as needed to prevent long-term use of bad network routing.

### 1.4 Channels

A channel is a named group of one or more clients which will allreceive messages addressed to that channel. The channel is created implicitly when the first client joins it, and the channel ceases to exist when the last client leaves it.  While channel exists, any client can reference the channel using the name of the channel.

* Channels names are strings (beginning with a '&' or '#' character) of length up to 200 characters
* the only restriction on a channel name is that it may not contain any spaces (' '), a control G (^G or ASCII 7), or a comma (',' which is used as a list item separator by the protocol).
* To create a new channel or become part of an existing channel, a user is required to JOIN the channel
* If the channel doesn't exist prior to joining, the channel is created and the creating user becomes a channel operator
* The __channel operator__ (also referred to as a "chop" or "chanop") on a given channel is considered to 'own' that channel. The commands which may only be used by channel operators are:
  * KICK    - Eject a client from the channel
  * MODE    - Change the channel's mode
  * INVITE  - Invite a client to an invite-only channel (mode +i)
  * TOPIC   - Change the channel topic in a mode +t channel
* A channel operator is identified by the '@' symbol next to their nickname whenever it is associated with a channel (ie replies to the NAMES, WHO and WHOIS commands).


## 2. The IRC Specification

### 2.2 Character codes

* No specific character set is specified. The protocol is based on a set of codes which are composed of eight (8) bits, making up an octet.
* Each message may be composed of any number of these octets however, some octet values are used for control codes which act as message delimiters.
* The characters {}| are considered to be the lower case equivalents of the characters []\, respectively. This is a critical issue when determining the equivalence of two nicknames.

### 2.3 Messages

Servers and clients send eachother messages which may or may not generate a reply. Client to server and server to server communication is essentially asynchronous in nature.

IRC messages are always lines of characters terminated with a CR-LF (Carriage Return - Line Feed) pair, and these messages shall not exceed 512 characters in length, counting all characters including the trailing CR-LF. Thus, there are 510 characters maximum allowed for the command and its parameters.  There is no provision for continuation message lines.

*Each IRC message may consist of up to three main parts:
  * The prefix (optional)
    * The presence of a prefix is indicated with a single leading ASCII colon character (':', 0x3b), which must be the first character of the message itself.  There must be no gap (whitespace) between the colon and the prefix. The prefix is used by servers to indicate the true origin of the message.  If the prefix is missing from the message, it is assumed to have originated from the connection from which it was received.
    * Clients should not use prefix when sending a message from themselves.
  * The command: Must either be a valid IRC command or a three (3) digit number represented in ASCII text.
  * The command parameters (of which there may be up to 15)
* The prefix, command, and all parameters are separated by one (or more) ASCII space character(s) (0x20).

The extracted message is parsed into the components <prefix>, <command> and list of parameters matched either by <middle> or <trailing> components.

The BNF representation for this is:

```bnf
<message>  ::= [':' <prefix> <SPACE> ] <command> <params> <crlf>
<prefix>   ::= <servername> | <nick> [ '!' <user> ] [ '@' <host> ]
<command>  ::= <letter> { <letter> } | <number> <number> <number>
<SPACE>    ::= ' ' { ' ' }
<params>   ::= <SPACE> [ ':' <trailing> | <middle> <params> ]

<middle>   ::= <Any *non-empty* sequence of octets not including SPACE
               or NUL or CR or LF, the first of which may not be ':'>
<trailing> ::= <Any, possibly *empty*, sequence of octets not including
                 NUL or CR or LF>

<crlf>     ::= CR LF
```
>
> NOTES
> ---
>
> 1. <SPACE> is consists only of SPACE character(s) (0x20). Specially notice that TABULATION, and all other control characters are considered NON-WHITE-SPACE.
> 2. After extracting the parameter list, all parameters are equal, whether matched by <middle> or <trailing>. <Trailing> is just a syntactic trick to allow SPACE within parameter.
> 3. The fact that CR and LF cannot appear in parameter strings is just artifact of the message framing. This might change later.
> 4. The NUL character is not special in message framing, and basically could end up inside a parameter, but as it would cause extra complexities in normal C string handling. Therefore NUL is not allowed >within messages.
> 5. The last parameter may be an empty string.
> 6. Use of the extended prefix (['!' <user> ] ['@' <host> ]) must not be used in server to server communications and is only intended for server to client messages in order to provide clients with more useful information about who a message is from without the need for additional queries.

### 2.4 Numeric replies

Most of the messages sent to the server generate a reply of some sort. The most common reply is the numeric reply, used for both errors and normal replies.

* The numeric reply must be sent as one message consisting of the sender prefix, the three digit numeric, and the target of the reply. A numeric reply is not allowed to originate from a client; any such messages received by a server are silently dropped.
*In all other respects, a numeric reply is just like a normal message, except that the keyword is made up of 3 numeric digits rather than a string of letters.

## 4. Message details

All commands described in this section must be implemented by any server for this protocol.

* If a full set of parameters is presented, then each must be checked for validity and appropriate responses sent back to the client.
* In the case of messages which use parameter lists using the comma as an item separator, a reply must be sent for each item.
### 2.2 Character codes

### 4.1 Connection Registration

The recommended order for a client to register is as follows:

1. Pass message
2. Nick message
3. User message

### 4.1.1 Password message

**Command:** PASS
**Parameters:** <password>

**Replies**:

* ERR_NEEDMOREPARAMS
* ERR_ALREADYREGISTRED

**Example**:

```
  PASS secretpasswordhere
```

* The PASS command is used to set a 'connection password'. The password can and must be set before any attempt to register the connection is made.  Currently this requires that clients send a PASS command before sending the NICK/USER combination and servers **must** send a PASS command before any SERVER command.
* The password supplied must match the one contained in the C/N lines (for servers) or I lines (for clients).  It is possible to send multiple PASS commands before registering but only the last one sent is used for verification and it may not be changed once registered.

### 4.1.2 Nick message

**Command:** NICK
**Parameters:** <nickname> [ <hopcount> ]

**Numeric Replies**:

* ERR_NONICKNAMEGIVEN
* ERR_ERRONEUSNICKNAME
* ERR_NICKNAMEINUSE
* ERR_NICKCOLLISION

**Example**:

```
NICK Wiz                        ; Introducing new nick "Wiz".
```

```
:WiZ NICK Kilroy                ; WiZ changed his nickname to Kilroy.
```

* NICK message is used to give user a nickname or change the previous one.
* The <hopcount> parameter is only used by servers to indicate how far away a nick is from its home server.  A local connection has a hopcount of 0.  If supplied by a client, it must be ignored.

### 4.1.3 User message

**Command:** USER
**Parameters:** <username> <hostname> <servername> <realname>

**Numeric Replies**:

* ERR_NEEDMOREPARAMS
* ERR_ALREADYREGISTRED

**Example**:

```
USER guest tolmoon tolsun :Ronnie Reagan
```

* The USER message is used at the beginning of connection to specify the username, hostname, servername and realname of a new user.
* It must be noted that realname parameter must be the last parameter, because it may contain space characters and must be prefixed with a colon (':') to make sure this is recognised as such.

### 4.1.4 Server message

**Command:** SERVER
**Parameters:** <servername> <hopcount> <info>

**Numeric Replies**:

* ERR_ALREADYREGISTRED

**Example**:

```
SERVER test.oulu.fi 1 :[tolsun.oulu.fi] Experimental server   ; New server test.oulu.fi introducing itself and attempting to register.  The name in []'s is the hostname for the host running test.oulu.fi.
```

* The SERVER message must only be accepted from either (a) a connection which is yet to be registered and is attempting to register as a server, or (b) an existing connection to another server, in  which case the SERVER message is introducing a new server behind that server.

### 4.1.5 Oper message

**Command:** OPER
**Parameters:** <user> <password>

**Numeric Replies**:

* ERR_NEEDMOREPARAMS
* RPL_YOUREOPER
* ERR_NOOPERHOST
* ERR_PASSWDMISMATCH

**Example**:

```
OPER foo bar                    ; Attempt to register as an operator
                                  using a username of "foo" and "bar" as
                                  the password.
```

*  OPER message is used by a normal user to obtain operator privileges. The combination of <user> and <password> are required to gain Operator privileges.
* If the client sending the OPER command supplies the correct password for the given user, the server then informs the rest of the network of the new operator by issuing a "MODE +o" for the clients nickname.

### 4.1.6 Quit message

**Command:** QUIT
**Parameters:** [<Quit message>]

**Numeric Replies**:

NONE

**Example**:

```
QUIT :Gone to have lunch        ; Preferred message format.
```

*  A client session is ended with a quit message.  The server must close the connection to a client which sends a QUIT message.

### 4.1.7 Server quit message

**Command:** SQUIT
**Parameters:** <server> <comment>

**Numeric Replies**:

* ERR_NOPRIVILEGES
* ERR_NOSUCHSERVER

**Example**:

```
SQUIT tolsun.oulu.fi :Bad Link ? ; the server link tolson.oulu.fi has
                                   been terminated because of "Bad Link"
```

```
:Trillian SQUIT cm22.eng.umd.edu :Server out of control
                                    ; message from Trillian to disconnect
                                   "cm22.eng.umd.edu" from the net
                                    because "Server out of control".
```


*  The SQUIT message is needed to tell about quitting or dead servers. If a server wishes to break the connection to another server it must send a SQUIT message to the other server, using the the name of the other server as the server parameter, which then closes its connection to the quitting server.

### 4.2 Channel operations

This group of messages is concerned with manipulating channels, their properties (channel modes), and their contents (typically clients). In implementing these, a number of race conditions are inevitable when clients at opposing ends of a network send commands which will ultimately clash.  It is also required that servers keep a nickname history to ensure that wherever a <nick> parameter is given, the server check its history in case it has recently been changed.

### 4.2.1 Join message

**Command:** JOIN
**Parameters:** <channel>{,<channel>} [<key>{,<key>}]

**Numeric Replies**:

* ERR_NEEDMOREPARAMS
* ERR_BANNEDFROMCHAN
* ERR_INVITEONLYCHAN
* ERR_BADCHANNELKEY
* ERR_CHANNELISFULL
* ERR_BADCHANMASK
* ERR_NOSUCHCHANNEL
* ERR_TOOMANYCHANNELS
* RPL_TOPIC

**Example**:

```
JOIN #foobar                    ; join channel #foobar.

JOIN &foo fubar                 ; join channel &foo using key "fubar".

JOIN #foo,&bar fubar            ; join channel #foo using key "fubar"
                               and &bar using no key.

JOIN #foo,#bar fubar,foobar     ; join channel #foo using key "fubar".
                               and channel #bar using key "foobar".

JOIN #foo,#bar                  ; join channels #foo and #bar.

:WiZ JOIN #Twilight_zone        ; JOIN message from WiZ
```

*  The JOIN command is used by client to start listening a specific channel
* Whether or not a client is allowed to join a channel is checked only by the server the client is connected to;
  1. the user must be invited if the channel is invite-only;
  2. the user's nick/username/hostname must not match any active bans;
  3. the correct key (password) must be given if it is set.
* Once a user has joined a channel, they receive notice about all commands their server receives which affect the channel.  This includes MODE, KICK, PART, QUIT and of course PRIVMSG/NOTICE.
* JOIN command needs to be broadcast to all servers so that each server knows where to find the users who are on the channel.
* If a JOIN is successful, the user is then sent the channel's topic (using RPL_TOPIC) and the list of users who are on the channel (using RPL_NAMREPLY), which must include the user joining.

### 4.2.2 Part message

**Command:** PART
**Parameters:** <channel>{,<channel>}

**Numeric Replies**:

* ERR_NEEDMOREPARAMS
* ERR_NOSUCHCHANNEL
* ERR_NOTONCHANNEL

**Example**:

```
PART #twilight_zone             ; leave channel "#twilight_zone"

PART #oz-ops,&group5            ; leave both channels "&group5" and
                                   "#oz-ops".
```

*  The PART message causes the client sending the message to be removed from the list of active users for all given channels listed in the parameter string.

### 4.2.3 Mode message

**Command:** MODE

For Channel modes:
---
**Parameters:** <channel> {[+|-]|o|p|s|i|t|n|b|v} [<limit>] [<user>] [<ban mask>]

For User modes:
---
**Parameters:** <nickname> {[+|-]|i|w|s|o}

**Numeric Replies**:

* ERR_NEEDMOREPARAMS
* RPL_CHANNELMODEIS
* ERR_CHANOPRIVSNEEDED
* ERR_NOSUCHNICK
* ERR_NOTONCHANNEL
* ERR_KEYSET
* RPL_BANLIST
* RPL_ENDOFBANLIST
* ERR_UNKNOWNMODE
* ERR_NOSUCHCHANNEL
* ERR_USERSDONTMATCH
* RPL_UMODEIS
* ERR_UMODEUNKNOWNFLAG

**Example of Channel Modes**:

```
MODE #Finnish +im               ; Makes #Finnish channel moderated and
                                'invite-only'.

MODE #Finnish +o Kilroy         ; Gives 'chanop' privileges to Kilroy on #Finnish

MODE #Finnish +v Wiz            ; Allow WiZ to speak on #Finnish.

MODE #Fins -s                   ; Removes 'secret' flag from channel
                                #Fins.

MODE #42 +k oulu                ; Set the channel key to "oulu".

MODE #eu-opers +l 10            ; Set the limit for the number of users
                                on channel to 10.

MODE &oulu +b                   ; list ban masks set for channel.

MODE &oulu +b *!*@*             ; prevent all users from joining.

MODE &oulu +b *!*@*.edu         ; prevent any user from a hostname
                                matching *.edu from joining.
```

```
:MODE WiZ -w                    ; turns reception of WALLOPS messages
                                off for WiZ.

:Angel MODE Angel +i            ; Message from Angel to make themselves
                                invisible.

MODE WiZ -o                     ; WiZ 'deopping' (removing operator
                                status).  The plain reverse of this
                                command ("MODE WiZ +o") must not be
                                allowed from users since would bypass
                                the OPER command.
```

*  The MODE command is provided so that channel operators may change the characteristics of `their' channel.  It is also required that servers be able to change channel modes so that channel operators may be created.
* The various modes available for channels are as follows:
  * o - give/take channel operator privileges;
  * p - private channel flag;
  * s - secret channel flag;
  * i - invite-only channel flag;
  * t - topic settable by channel operator only flag;
  * n - no messages to channel from clients on the outside;
  * m - moderated channel;
  * l - set the user limit to channel;
  * b - set a ban mask to keep users out;
  * v - give/take the ability to speak on a moderated channel;
  * k - set a channel key (password).

* The user MODEs are typically changes which affect either how the client is seen by others or what 'extra' messages the client is sent. A user MODE command may only be accepted if both the sender of the message and the nickname given as a parameter are both the same.
* The available modes are as follows:
  * i - marks a users as invisible;
  * s - marks a user for receipt of server notices;
  * w - user receives wallops;
  * o - operator flag.


### 4.2.4 Topic message

**Command:** TOPIC
**Parameters:** <channel> [<topic>]

**Numeric Replies**:

* ERR_NEEDMOREPARAMS
* ERR_NOTONCHANNEL
* RPL_NOTOPIC
* RPL_TOPIC
* ERR_CHANOPRIVSNEEDED

**Example**:

```
:Wiz TOPIC #test :New topic     ;User Wiz setting the topic.

TOPIC #test :another topic      ;set the topic on #test to "another
                               topic".

TOPIC #test                     ; check the topic for #test.
```

*  The TOPIC message is used to change or view the topic of a channel. The topic for channel <channel> is returned if there is no <topic> given.  If the <topic> parameter is present, the topic for that channel will be changed, if the channel modes permit this action.

### 4.2.5 Names message

**Command:** NAMES
**Parameters:** [<channel>{,<channel>}]

**Numeric Replies**:

* RPL_NAMREPLY
* RPL_ENDOFNAMES

**Example**:

```
NAMES #twilight_zone,#42        ; list visible users on #twilight_zone
                                   and #42 if the channels are visible to
                                   you.

NAMES                           ; list all visible channels and users
```

* A user can list all nicknames that are visible to them on any channel that they can see.
* Channel names which they can see are those which aren't private (+p) or secret (+s) or those which they are actually on
* If no <channel> parameter is given, a list of all channels and their occupants is returned.  At the end of this list, a list of users who  are visible but either not on any channel or not on a visible channel are listed as being on `channel' "*".

### 4.2.6 List message

SEE [RFC 1459](https://tools.ietf.org/html/rfc1459#section-1.3.1)

### 4.2.7 Invite message

SEE [RFC 1459](https://tools.ietf.org/html/rfc1459#section-1.3.1)

### 4.2.8 Kick command

SEE [RFC 1459](https://tools.ietf.org/html/rfc1459#section-1.3.1)


## 4.4 Sending Messages

PRIVMSG and NOTICE are the only messages available which actually perform delivery of a text message from one client to another - the rest just make it possible and try to ensure it happens in a reliable and structured manner.

### 4.4.1 Private messages

**Command:** PRIVMSG
**Parameters:** <receiver>{,<receiver>} <text to be sent>

**Replies**:

* ERR_NORECIPIENT
* ERR_NOTEXTTOSEND
* ERR_CANNOTSENDTOCHAN
* ERR_NOTOPLEVEL
* ERR_WILDTOPLEVEL
* ERR_TOOMANYTARGETS
* ERR_NOSUCHNICK
* RPL_AWAY

**Example**:

```
:Angel PRIVMSG Wiz :Hello are you receiving this message ?
                              ; Message from Angel to Wiz.

PRIVMSG Angel :yes I'm receiving it !receiving it !'u>(768u+1n) .br ;
                                Message to Angel.

PRIVMSG jto@tolsun.oulu.fi :Hello !
                                ; Message to a client on server
                                tolsun.oulu.fi with username of "jto".

PRIVMSG $*.fi :Server tolsun.oulu.fi rebooting.
                                ; Message to everyone on a server which
                                has a name matching *.fi.

PRIVMSG #*.edu :NSFNet is undergoing work, expect interruptions
                                ; Message to all users who come from a
                                host which has a name matching *.edu.
```

* PRIVMSG is used to send private messages between users.  <receiver> is the nickname of the receiver of the message.  <receiver> can also be a list of names or channels separated with commas.

### 4.4.2 Notice messages

**Command:** NOTICE
**Parameters:** <nickname> <text>

See PRIVMSG for more details on replies and examples.

The NOTICE message is used similarly to PRIVMSG.  The difference
between NOTICE and PRIVMSG is that automatic replies must never be
sent in response to a NOTICE message.
