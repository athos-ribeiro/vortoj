FILES = main.c modules/*.c include/*.h
#INPUT = main.c modules/*.c
INPUT = main.c
FLAGS = -Wall -g -pthread -ansi -pedantic
OUTPUT = server

CONFIG_FILE = config.txt
PORT = `head -1 $(CONFIG_FILE) | tail -1`
MAXIMUM_NUMBER_OF_CLIENTS = `head -2 $(CONFIG_FILE) | tail -1`

all: execute

compile: $(FILES)
	gcc -o $(OUTPUT) $(FLAGS) $(INPUT)

execute: compile
	./server $(SERVER) $(PORT) $(MAXIMUM_NUMBER_OF_CLIENTS)

clean:
	rm -f server
