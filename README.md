# About Vortoj
Vortoj is a very simple IRC Server implemented over [RFC 1459](https://tools.ietf.org/html/rfc1459#section-1.3.1) that it's gonna be developed during a Computer Network Programming discipline by Arthur Del Esposte and Athos Ribeiro.

# Compile and Run

In order to properly use Vortoj IRC Server you need to write the correct parameters inside config.txt file. This file must have two lines, as one can see in the following example:

```
PORT_NUMBER
MAXIMUM_NUMBER_OF_CLIENTS
```

These parameters are going to be used as arguments while running the server.

After this, you can use the makefile to compile and run the server or, if you want, only compile the source code:

* To compile and run: `$ make`
* To only compile: `$ make compile`

# Technical References

All technical references can be found into the following files:

* **[irc-rfc-compilation.md](irc-rfc-compilation.md)**: it's a compilation of RFC 1459 with the most important things related to this work.

# Authors

* Arthur de Moura Del Esposte <arthurmde at gmail dot com || esposte at ime dot usp dot br>
* Athos Coimbra Ribeiro <athoscr at ime dot usp dot br>

# License

None yet.

